const nbonnaci = (n: number, m: number) => {
  let serie = Array(m).fill(0);
  for (let i = 0; i < m; i++) {
    serie[i] = 0;
  }
  serie[n - 1] = 1;
  serie[n] = 1;

  for (let i = n + 1; i < m; i++) {
    serie[i] = 2 * serie[i - 1] - serie[i - n - 1];
  }

  return serie;
};

console.log(nbonnaci(4, 10));
// nbonnaci(4, 10);
