let reservations = JSON.parse(window.localStorage.getItem("reservations"));
console.log(reservations)
let table = document.getElementsByTagName("table")[0];

for (let i = 0; i < reservations.length; i++) {
  const reservation = reservations[i];
  console.log("el", reservation);
  let tr = document.createElement("tr");
  //arrivaldate
  let tdArrival = document.createElement("td");
  let txtArrival = document.createTextNode(reservation.arrivalDate);
  tdArrival.appendChild(txtArrival);
  tr.appendChild(tdArrival);
  //nights
  let tdNights = document.createElement("td");
  let txtNights = document.createTextNode(reservation.nights);
  tdNights.appendChild(txtNights);
  tr.appendChild(tdNights);
  //adults
  let tdAdults = document.createElement("td");
  let txtAdults = document.createTextNode(reservation.adults);
  tdAdults.appendChild(txtAdults);
  tr.appendChild(tdAdults);
  //children
  let tdChildren = document.createElement("td");
  let txtChildren = document.createTextNode(reservation.children);
  tdChildren.appendChild(txtChildren);
  tr.appendChild(tdChildren);
  //room type
  let tdRoom = document.createElement("td");
  let txtRoom = document.createTextNode(reservation?.roomType);
  tdRoom.appendChild(txtRoom);
  tr.appendChild(tdRoom);
  //bed type
  let tdBed = document.createElement("td");
  let txtBed = document.createTextNode(reservation?.roomType);
  tdBed.appendChild(txtBed);
  tr.appendChild(tdBed);
  //smoking
  let tdSmoking = document.createElement("td");
  let smoking = document.createTextNode(reservation?.smoking);
  let image = document.createElement("img");
  image.className = "smoking-img";


  if(smoking.data==='true'){
      image.src ="/assets/icons/smoking-solid.svg"
  }else {
      image.src = "/assets/icons/ban-smoking-solid.svg";
  }

  tdSmoking.appendChild(image);
  tr.appendChild(tdSmoking);

  //name
  let tdName = document.createElement("td");
  let txtName = document.createTextNode(reservation?.name);
  tdName.appendChild(txtName);
  tr.appendChild(tdName);

  //email
  let tdEmail = document.createElement("td");
  let txtEmail = document.createTextNode(reservation?.email);
  tdEmail.appendChild(txtEmail);
  tr.appendChild(tdEmail);

  //phone
  let tdPhone = document.createElement("td");
  let txtPhone = document.createTextNode(reservation?.phone);
  tdPhone.appendChild(txtPhone);
  tr.appendChild(tdPhone);

  //button remove
  let tdRemoveBtn = document.createElement("td");
  var span = document.createElement("span");
  span.innerHTML =
    '<button id="' +
    i +
    '" onclick="removeReservation(this.id)" class="btn-remove" >Remove </button>';

  //add btn
  tdRemoveBtn.appendChild(span);
  tr.appendChild(tdRemoveBtn);
  table.appendChild(tr);
}

function removeReservation(id) {
  reservations.splice(id,1);
  window.localStorage.setItem("reservations", JSON.stringify(reservations));

  console.log("hola", reservations);
  location.reload()
}
