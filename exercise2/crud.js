
document.addEventListener("submit", (e) => {
  e.preventDefault();
  save_data();
});

function save_data() {
  let arrivalDate = document.getElementById("arrival-date");
  console.log(arrivalDate.value);
  let nights = document.getElementById("nights");
  let adults = document.getElementById("adults");
  let children = document.getElementById("children");


  let radioRoomType = document.querySelectorAll('input[name="room-type"]');
  for (let i = 0; i < radioRoomType.length; i++) {
    if (radioRoomType[i].checked) {
      var selectedRoom = radioRoomType[i].value;
    }
  }
  let radioBedType = document.querySelectorAll('input[name="room-type"]');

  for (let i = 0; i < radioBedType.length; i++) {
    if (radioBedType[i].checked) {
      var selectedBed = radioBedType[i].value;
    }
  }


  let smoking = false;
  if (document.getElementById("smoking").checked) {
    smoking = true;
  }

  let name = document.getElementById("name");
  let email = document.getElementById("email");
  let phone = document.getElementById("phone");

  const reservation = {
    arrivalDate: arrivalDate?.value,
    nights: nights?.value,
    adults: adults?.value,
    children: children?.value,
    roomType: selectedRoom,
    bedType: selectedBed,
    smoking: smoking,
    name: name?.value,
    email: email?.value,
    phone: phone?.value,
  };
  console.log("reservation to save=>", reservation);
  let reservations = JSON.parse(window.localStorage.getItem("reservations"));
  if (!reservations) {
    window.localStorage.setItem("reservations", "[]");
    reservations = JSON.parse(window.localStorage.getItem("reservations"));
  }
  console.log("R=>", reservations);
  reservations.push(reservation);

  window.localStorage.setItem("reservations", JSON.stringify(reservations));
  location.reload();
}
