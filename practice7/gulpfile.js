// var browserSync = require("browser-sync");
// var gulp = require("gulp");
// var server = require("gulp-webserver");
// var autoprefixer = require("gulp-autoprefixer");
// var sass = require("gulp-sass")(require("sass"));
// var concat = require("gulp-concat");
// var GulpCleanCss = require("gulp-clean-css");
// var GulpUglify = require("gulp-uglify");
// var imagemin = require("gulp-imagemin");
// var inject = require("gulp-inject");
import gulp from "gulp";
import minifyCSS from "gulp-minify-css";
const { series, parallell, src, dest, task } = gulp;
// import { sass } from "gulp-sass";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import GulpCleanCss from "gulp-clean-css";
import uglify from "gulp-uglify";
import imagemin from "gulp-imagemin";
import inject from "gulp-inject";
import autoPrefixer from "gulp-autoprefixer";
import concat from "gulp-concat";
import server from "gulp-webserver";
import browserSync from "browser-sync";
// import uglify
const PORT = 4042;
// server
gulp.task("server", () => {
  gulp.src("./dist").pipe(
    server({
      livereaload: true,
      open: true,
      port: PORT,
    })
  );
  browserSync.reload();
});

gulp.task("build", function (cb) {
  gulp
    .src("index.html")
    .pipe(inject(gulp.src(["./dist/*.js", "./dist/*.css"], { read: false })))
    .pipe(gulp.dest("dist/"));
  gulp
    .src("css/*.sass")
    .pipe(sass().on("error", sass.logError))
    .pipe(concat("css/styles.css"))
    .pipe(autoPrefixer({ cascade: true }))
    .pipe(GulpCleanCss({ compatibility: "*" }))
    .pipe(browserSync.stream())
    .pipe(gulp.dest("dist/"));

  gulp.src("img/*").pipe(imagemin()).pipe(gulp.dest("dist/img"));
  gulp
    .src("js/*.js")
    .pipe(concat("js/counter.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/"));
  cb();
});

// function styles() {
//   console.log("styles");
//   return gulp
//     .src("css/*.sass")
//     .pipe(sass().on("error", sass.logError))
//     .pipe(concat("css/styles.css"))
//     .pipe(autoPrefixer({ cascade: true }))
//     .pipe(GulpCleanCss({ compatibility: "*" }))
//     .pipe(gulp.dest("dist/"));
//   // .pipe(browserSync.stream());
// }
// function js() {
//   return gulp
//     .src("js/*.js")
//     .pipe(concat("js/counter.js"))
//     .pipe(GulpUglify().on("error", GulpUglify.logError))
//     .pipe(gulp.dest("dist/"));
// }
// function html() {
//   return gulp
//     .src("index.html")
//     .pipe(inject(gulp.src(["./dist/*.js", "./dist/*.css"], { read: false })))
//     .pipe(gulp.dest("dist/"));
// }
// function img() {
//   return gulp.src("img/*").pipe(imagemin()).pipe(gulp.dest("dist/assets"));
// }

gulp.task("reload", function () {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
    port: PORT,
  });
});

gulp.task("watch", function (done) {
  gulp.watch(
    "index.html",
    series("build", function () {
      browserSync.reload();
      cb();
    })
  );
  gulp.watch(
    "js/*.js",
    series("build", function () {
      browserSync.reload();
      cb();
    })
  );
  gulp.watch(
    "css/*.sass",
    series("build", function (cb) {
      browserSync.reload();
      cb();
    })
  );
  done();
});

gulp.task("go", gulp.series("watch", "server"));

//another
gulp.task("onlyWatch", (done) => {
  gulp.watch("index.html", gulp.series("build"));
  gulp.watch("js/*.js", gulp.series("build"));
  gulp.watch("css/*.css/*.sass", gulp.series("build"));
  done();
});
gulp.task("onlyWatchServer", gulp.series("onlyWatch", "server"));
