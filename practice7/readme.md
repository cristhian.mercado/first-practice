Practica de task runners con gulp
<Desripcion>
Put your logic inside the js folder, all the styles inside css, also use a preprocessor, and at
least one image inside img folder.
a. Create a gulpfile and a package.json
b. Create a task to start a new server
c. Create a build task to move all the content into a dist folder, following these
requirements:

i. Convert all the scss into a single css file
ii. Add an auto prefix to your styles
iii. Minify the new css file
iv. Convert all the js into a single js file
v. Uglify the generated js
vi. Minify all the images and move into dist folder
vii. Inject automatically the generated files into index.html and move to dist
folder

d. Create a watch task and listen all the changes over your js, scss and html files, on
every update do a build and reload the browser
e. Create a serve task that will build, watch and start the server
