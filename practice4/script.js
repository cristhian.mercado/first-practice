let body = document.getElementsByTagName('body');
console.log(body[0].className)

function changeTheme(){
    let body = document.getElementsByTagName('body');
    let iconTheme = document.getElementById("icon-theme")
    let btnTheme = document.getElementById("btn-text")

    if(body[0].className==='dark'){
        body[0].classList.remove("dark");
        iconTheme.src ="/assets/icons/moon-solid.svg";
        btnTheme.textContent ="Dark mode"

    }else{
        body[0].className = "dark"
        btnTheme.textContent ="Light mode"
        iconTheme.src ="/assets/icons/sun-solid.svg";
    }
    console.log(body[0].className)
}