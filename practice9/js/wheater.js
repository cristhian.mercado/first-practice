// import { API_URL, CBBA, CLIENT_ID } from "./constants";
const API_URL = "https://openweathermap.org/";
const CLIENT_ID = "7889565148a7f26fbdca2ad90300c0b0";

const coordenadas = {
  cbba: {
    lat: -17.414,
    long: -66.1653,
  },
  lapaz: {
    lat: -16.5,
    long: -68.15,
  },
  scz: {
    lat: -17.8146,
    long: -63.1561,
  },
  potosi: {
    lat: -17.414,
    long: -66.1653,
  },
  oruro: {
    lat: -17.414,
    long: -66.1653,
  },
  chuquisaca: {
    lat: -17.414,
    long: -66.1653,
  },
  tarija: {
    lat: -17.414,
    long: -66.1653,
  },
  pando: {
    lat: -17.414,
    long: -66.1653,
  },
  beni: {
    lat: -17.414,
    long: -66.1653,
  },
};

const fetchWeather = async (city) => {
  return await fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${CLIENT_ID}`,
    { method: "GET" }
  )
    .then(async (response) => {
      let data = await response.json();
      renderWeather(data);
    })
    .catch((error) => console.log("error", error));
};

fetchWeather("cochabamba");

const renderWeather = (w) => {
  let loader = document.getElementsByClassName("loader");
  console.log(w);
  let iconurl = "http://openweathermap.org/img/w/" + w.weather[0].icon + ".png";
  let title = document.getElementsByClassName("title");
  let nro = document.getElementsByClassName("summary__temp__nro");
  let img = document.getElementsByClassName("summary__temp__img");

  let prob = document.getElementsByClassName("more__text__prob");
  let humidity = document.getElementsByClassName("more__text__humidity");
  let wind = document.getElementsByClassName("more__text__wind");
  let citieTempmax = document.getElementsByClassName("more__text__tempmax");
  let citieTempmin = document.getElementsByClassName("more__text__tempmin");

  let citieDate = document.getElementsByClassName("citie__date");
  let citieCord = document.getElementsByClassName("citie__cord");

  let citieName = document.getElementsByClassName("citie__name");
  let citieWeather = document.getElementsByClassName("citie__weather");
  img[0].src = iconurl;
  title[0].textContent = `Esta viendo datos de su ciudad local: ${w.name}`;
  nro[0].textContent = w.main.temp;
  prob[0].textContent = `Prob. de precipitación: ${w.main.pressure}%`;

  humidity[0].textContent = `Humedad: ${w.main.humidity}%`;
  wind[0].textContent = `Viento: a ${w.wind?.speed} km/h`;
  citieName[0].textContent = w.name;
  citieDate[0].textContent = new Date((w.sys.sunrise + w.timezone) * 1000);
  citieWeather[0].textContent = w.weather[0].description;
  console.log(citieCord);
  citieCord[0].textContent = `lat: ${w.coord.lat}, lon: ${w.coord.lon}`;
  citieTempmax[0].textContent = `Temp. maxima: ${w.main.temp_max}`;
  citieTempmin[0].textContent = `Temp. minima: ${w.main.temp_min}`;
  loader[0].remove("not-visible");
};

const w = {
  coord: {
    lon: 10.99,
    lat: 44.34,
  },
  weather: [
    {
      id: 501,
      main: "Rain",
      description: "moderate rain",
      icon: "10d",
    },
  ],
  base: "stations",
  main: {
    temp: 298.48,
    feels_like: 298.74,
    temp_min: 297.56,
    temp_max: 300.05,
    pressure: 1015,
    humidity: 64,
    sea_level: 1015,
    grnd_level: 933,
  },
  visibility: 10000,
  wind: {
    speed: 0.62,
    deg: 349,
    gust: 1.18,
  },
  rain: {
    "1h": 3.16,
  },
  clouds: {
    all: 100,
  },
  dt: 1661870592,
  sys: {
    type: 2,
    id: 2075663,
    country: "IT",
    sunrise: 1661834187,
    sunset: 1661882248,
  },
  timezone: 7200,
  id: 3163858,
  name: "Zocca",
  cod: 200,
};
