let number1 = 0;
let number2 = 0;
let operator = "";
let result;

const touch = (character) => {
  let number = document.getElementsByClassName("calc__input-text")[0];
  if (number.textContent === "0") {
    number.textContent = character;
  } else {
    number.textContent += character;
  }
};

const clearAll = () => {
  document.getElementsByClassName("calc__input-text")[0].textContent = 0;
  number1 = 0;
  number2 = 0;
  operator = "";
  result = "";
};

const operation = (op) => {
  // let number = document.getElementsByClassName("calc__input-text")[0];
  let input = document.getElementsByClassName("calc__input-text")[0];
  number1 = input.textContent;
  if (input.textContent !== "0") {
    touch(operator);
  }
  operator = op;
};

const calculate = () => {
  let input = document.getElementsByClassName("calc__input-text")[0];
  let array = input.textContent.split(operator);
  number2 = array[1];
  if (operator === "+") {
    result = sumar(number1, number2);
  } else if (operator === "*") {
    result = multiply(number1, number2);
  } else if (operator === "/") {
    result = divide(number1, number2);
  } else if (operator === "-") {
    result = rest(number1, number2);
  }
  input.textContent = result;
};

const sumar = (a, b) => {
  return parseFloat(a) + parseFloat(b);
};
const multiply = (a, b) => {
  return parseFloat(a) * parseFloat(b);
};

const divide = (a, b) => {
  return parseFloat(a) / parseFloat(b);
};
const rest = (a, b) => {
  return parseFloat(a) - parseFloat(b);
};

module.exports = {
  sumar,
  multiply,
  rest,
  divide,
  operation,
  calculate,
  clearAll,
};
