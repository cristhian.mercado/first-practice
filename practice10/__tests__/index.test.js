const {
  sumar,
  divide,
  rest,
  multiply,
  touch,
  clearAll,
  calculate,
} = require("../src/js/index");

describe("math functions", () => {
  test("sumar should return the sum of two numbers", () => {
    const result = sumar(5, 10);
    expect(result).toBe(15);
    console.log(result);
  });
  test("multiply should return the product of two numbers", () => {
    const result = multiply(5, 10);
    expect(result).toBe(50);
  });
  test("divide should return the division of two numbers", () => {
    const result = divide(4, 2);
    expect(result).toBe(2);
  });
  test("rest should return the difference of two numbers", () => {
    const result = rest(10, 5);
    expect(result).toBe(5);
  });
});

// describe("display number touched", () => {});

// describe("display 0 when touch clear button", () => {
//   let inputText;
//   let number1 = 22;
//   let number2 = 2;
//   let operator = "+";
//   let result;

//   beforeEach(() => {
//     inputText = document
//       .createElement("p")
//       .className("calc__input-text")[0].textContent =
//       number1 + operator + number2;
//   });
//   it("deberia mostrar 0", () => {
//     expect(clearAll().toBe("0"));
//   });
// });
